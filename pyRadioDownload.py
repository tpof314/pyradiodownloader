import urllib
import string
import thread

def getUrlList(channelID, start, end):
    # hz,
    sub_url = 'http://m.hz.qingting.fm/cache?'
    url = sub_url + 'id=' + str(channelID) + '&start=' + start + '&end=' + end
    print url
    strList = urllib.urlopen(url).read()
    list = string.split(strList, '\n')
    for item in list:
        head = item[0:1]
        if head == '#':
            list.remove(item)
    return list

def downloadFragment(file_header, list):
    count = 0
    for item in list:
        try:
            print 'downloading ' + item
            filename = file_header + str(count).zfill(7) + '.aac'
            urllib.urlretrieve(item, filename)
            count = count + 1
        except:
            continue
'''
def mergeFiles(file_header):
    import os
    command1 = 'type ' + file_header + '* > final_' + file_header + '.aac'
    command2 = 'del ' + file_header + '*'
    command3 = 'ren final_' + file_header + '.aac ' + file_header + '.aac'
    os.system(command1)
    os.system(command2)
    os.system(command3)
'''

def mergeFiles(file_header):
    import os
    command1 = 'cat ' + file_header + '* > final_' + file_header + '.aac'
    command2 = 'rm ' + file_header + '*'
    command3 = 'mv final_' + file_header + '.aac ' + file_header + '.aac'
    os.system(command1)
    os.system(command2)
    os.system(command3)

start = '14M09D11h09m01s00'
end = '14M09D11h09m29s00'
list = getUrlList(4594, start, end)
file_header = 'Chaozhou' + start
downloadFragment(file_header, list)
mergeFiles(file_header)
